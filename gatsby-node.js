const path = require("path")

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  const pages = await graphql(`
    {
      allPrismicPost {
        edges {
          node {
            id
            uid
          }
        }
      }
      allPrismicProject {
        edges {
          node {
            id
            uid
          }
        }
      }
    }
  `)

  //   Setup posts component
  const blogTemplate = path.resolve("src/templates/post.jsx")

  pages.data.allPrismicPost.edges.forEach(edge => {
    createPage({
      path: `blog/${edge.node.uid}`,
      component: blogTemplate,
      context: {
        uid: edge.node.uid,
      },
    })
  })

  //   Setup projects components
  const projectTemplate = path.resolve("src/templates/project.jsx")

  pages.data.allPrismicProject.edges.forEach(edge => {
    createPage({
      path: `projects/${edge.node.uid}`,
      component: projectTemplate,
      context: {
        uid: edge.node.uid,
      },
    })
  })
}

import React from "react"
import styled from "@emotion/styled"
import theme from "../styles/theme"

import AppninDiscos from "../images/clients/appnindiscos.svg"
import Flo from "../images/clients/flo.svg"
import Marketable from "../images/clients/marketable.svg"
import Oolu from "../images/clients/oolu.svg"
import Pharmaseal from "../images/clients/pharmaseal.svg"
import Scape from "../images/clients/scape.svg"
import Sunesis from "../images/clients/sunesis.svg"
import Wilmott from "../images/clients/wilmott.svg"
import Ydentity from "../images/clients/ydentity.svg"

const CompanyGridContainer = styled.div`
  width: 100%;
  max-width: ${theme.maxWidth};
  padding: ${theme.gutter};
  align-items: center;
  margin-left: auto;
  margin-right: auto;
`
const CompanyImageGrid = styled.div`
  width: 100%;
  margin-top: auto;
  margin-bottom: auto;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  @media (max-width: ${theme.breakpoints.md}) {
    margin-bottom: 16px;
  }
`
const CompanyImage = styled.img`
  width: 100%;
  max-width: 180px;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 0;
  max-height: 160px;
`
const CompanyGridText = styled.div`
  text-align: center;
`

const CompanyGrid = props => {
  return (
    <>
      <CompanyGridContainer>
        <CompanyGridText>{props.children}</CompanyGridText>
        <CompanyImageGrid>
          <CompanyImage src={AppninDiscos} alt="Appnin Discos" />
          <CompanyImage src={Flo} alt="Flo Skatepark" />
          <CompanyImage src={Marketable} alt="We Are Marketable" />
          <CompanyImage src={Oolu} alt="Oolu Insure" />
          <CompanyImage src={Pharmaseal} alt="Pharmaseal" />
          <CompanyImage src={Scape} alt="Scape Group" />
          <CompanyImage src={Sunesis} alt="Sunesis" />
          <CompanyImage src={Wilmott} alt="Wilmott Dixons" />
          <CompanyImage src={Ydentity} alt="Ydentity" />
        </CompanyImageGrid>
      </CompanyGridContainer>
    </>
  )
}

export default CompanyGrid

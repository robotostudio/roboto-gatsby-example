import React from "react"
import styled from "@emotion/styled"
import theme from "../styles/theme"

import Gatsby from "../images/partners/gatsby.svg"
import Netlify from "../images/partners/netlify.svg"
import Prismic from "../images/partners/prismic.svg"

const StackFeatureContainer = styled.div`
  width: 100%;
  max-width: ${theme.maxWidth};
  padding: ${theme.gutter};
  align-items: center;
  margin-left: auto;
  margin-right: auto;
  @media (min-width: ${theme.breakpoints.md}) {
    display: grid;
    grid-gap: 32px;
    grid-auto-flow: dense;
    align-items: flex-start;
    grid-template-columns: 2fr 3fr;
  }
`
const StackDescription = styled.div`
  @media (max-width: ${theme.breakpoints.md}) {
    margin-bottom: 16px;
  }
`
const StackFeatureText = styled.div`
  width: 100%;
  ol {
    counter-reset: stackCount;
    margin: 0;
    li {
      list-style: none;
      padding-left: 56px;
      position: relative;
      &:before {
        content: counter(stackCount);
        position: absolute;
        color: ${theme.colors.hotPink};
        counter-increment: stackCount;
        left: 0;
        top: 0;
        width: 32px;
        height: 32px;
        text-align: center;
        line-height: 32px;
      }
      &:after {
        content: "";
        position: absolute;
        border: ${theme.colors.hotPink} solid 1px;
        width: 32px;
        height: 32px;
        border-radius: 32px;
        left: 0;
        top: 0;
      }
    }
  }
`

const StackImages = styled.div`
  img {
    max-height: 24px;
    opacity: 0.4;
    + img {
      margin-left: 16px;
    }
  }
`

const StackFeature = props => {
  return (
    <>
      <StackFeatureContainer>
        <StackDescription>
          <StackImages>
            <img src={Gatsby} alt="Gatsby Logo" />
            <img src={Prismic} alt="Prismic Logo" />
            <img src={Netlify} alt="Netlify Logo" />
          </StackImages>
          <h2>Custom Development</h2>
          <p>
            We build your website out from scratch, using the best development
            practices freshly shipped from Silicon Valley. There’s no corners
            cuts, no compormises and especially no restrictions, we can build
            anything you like.
          </p>
          <p>
            We use our cutting edge, scalable stack of Prismic, Gatsby &amp;
            Netlify, to deliver blazing fast web experience. We don’t do half
            baked, amateur hour sites - we’ve got a serious need for speed
          </p>
        </StackDescription>
        <StackFeatureText>
          <ol>
            <li>
              We start by setting up a collaborative document that you fill with
              all the content you can to help us understand your vision
            </li>
            <li>
              Then we start with wireframing your website in a primitive form,
              understanding some of the key concepts, and metrics that you want
              to achieve
            </li>
            <li>
              We then move to developing a design system: think of this as a
              functional brand guideline that is used to produce your website,
              and provide an incredible foundation for future projects
            </li>
            <li>
              We then move to developing a design system: think of this as a
              functional brand guideline that is used to produce your website,
              and provide an incredible foundation for future projects
            </li>
          </ol>
        </StackFeatureText>
      </StackFeatureContainer>
    </>
  )
}

export default StackFeature

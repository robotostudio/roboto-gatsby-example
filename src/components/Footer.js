import React from "react"
import styled from "@emotion/styled"
import theme from "../styles/theme"
import Prismic from "../images/partners/prismic.svg"
import Gatsby from "../images/partners/gatsby.svg"
import Netlify from "../images/partners/netlify.svg"

const FooterOuter = styled.footer`
  width: 100%;
  margin-top: 64px;
  padding-top: ${theme.gutter};
  padding-bottom: ${theme.gutter};
  display: flex;
  justify-content: center;
  align-items: center;
`
const FooterInner = styled.div`
  width: 100%;
  max-width: ${theme.maxWidth};
  padding-right: ${theme.gutter};
  padding-left: ${theme.gutter};
`
const FooterCtaContainer = styled.div`
  @media (min-width: ${theme.breakpoints.sm}) {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    row-gap: 64px;
  }
`
const FooterCta = styled.div`
  margin-bottom: 16px;
  h3 {
    margin-bottom: 8px;
  }
`
const FooterCtaLink = styled.a`
  color: ${theme.colors.hotPink};
  text-decoration: underline;
`
const PartnerContainer = styled.div`
  grid-column: 1 / 4;
  display: flex;
  justify-content: space-between;
`
const PartnerLogo = styled.img`
  opacity: 0.2;
  max-height: 32px;
  @media (max-width: ${theme.breakpoints.sm}) {
    display: none;
  }
`

const Footer = () => {
  return (
    <>
      <FooterOuter>
        <FooterInner>
          <FooterCtaContainer>
            <FooterCta>
              <h3>Send us an email</h3>
              <FooterCtaLink href="mailto:yo@roboto.studio">
                yo@roboto.studio
              </FooterCtaLink>
            </FooterCta>
            <FooterCta>
              <h3>Have a chat</h3>
              <FooterCtaLink href="tel:01158821993">01158821993</FooterCtaLink>
            </FooterCta>
            <FooterCta>
              <h3>Got a project?</h3>
              <FooterCtaLink>Let's figure it out</FooterCtaLink>
            </FooterCta>
            <PartnerContainer>
              <PartnerLogo src={Prismic} alt="Prismic" />
              <PartnerLogo src={Gatsby} alt="Gatsby" />
              <PartnerLogo src={Netlify} alt="Netlify" />
            </PartnerContainer>
          </FooterCtaContainer>
          © {new Date().getFullYear()}, Built with
          {` `}
          <a href="https://www.gatsbyjs.org">Gatsby</a>
        </FooterInner>
      </FooterOuter>
    </>
  )
}

export default Footer

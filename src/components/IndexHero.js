import React from "react"
import styled from "@emotion/styled"
import theme from "../styles/theme"

const HeroText = styled.h1`
  text-align: center;
  width: 100%;
`
const HeroSubText = styled.h3`
  text-align: center;
  width: 100%;
  opacity: 0.4;
`
const IndexHeroContainer = styled.div`
  width: 100%;
  max-width: ${theme.maxWidth};
  padding: ${theme.gutter};
  display: flex;
  flex-wrap: wrap;
  align-content: center;
  margin-left: auto;
  margin-right: auto;
  height: calc(100vh - 112px);
`

const IndexHero = props => {
  return (
    <>
      <IndexHeroContainer>
        <HeroText>
          Lightning fast websites, built with the best technologies
        </HeroText>
        <HeroSubText>Based in sunny old Nottingham</HeroSubText>
      </IndexHeroContainer>
    </>
  )
}

export default IndexHero

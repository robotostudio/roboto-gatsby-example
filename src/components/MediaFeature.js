import React from "react"
import styled from "@emotion/styled"
import theme from "../styles/theme"
import Image from "./image"

const MediaFeatureContainer = styled.div`
  width: 100%;
  max-width: ${theme.maxWidth};
  padding: ${theme.gutter};
  align-items: center;
  margin-left: auto;
  margin-right: auto;
  @media (min-width: ${theme.breakpoints.md}) {
    display: grid;
    grid-gap: 32px;
    grid-auto-flow: dense;
    grid-template-columns: ${props => (props.reverse ? "3fr 2fr" : "2fr 3fr")};
  }
`
const MediaFeatureImage = styled.div`
  width: 100%;
  margin-top: auto;
  margin-bottom: auto;
  @media (max-width: ${theme.breakpoints.md}) {
    margin-bottom: 16px;
  }
  @media (min-width: ${theme.breakpoints.md}) {
    grid-column: ${props => (props.reverse ? "1" : "2")};
  }
`
const MediaFeatureText = styled.div`
  width: 100%;
  @media (min-width: ${theme.breakpoints.md}) {
    grid-column: ${props => (props.reverse ? "2" : "1")};
  }
`

const MediaFeature = props => {
  return (
    <>
      <MediaFeatureContainer reverse={props.reverse}>
        <MediaFeatureImage reverse={props.reverse}>
          <Image filename={props.filename} alt={props.alt}></Image>
        </MediaFeatureImage>
        <MediaFeatureText reverse={props.reverse}>
          {props.children}
        </MediaFeatureText>
      </MediaFeatureContainer>
    </>
  )
}

export default MediaFeature

import React from "react"
import styled from "@emotion/styled"
import theme from "../styles/theme"
import LogoImg from "../images/brand/logo.svg"
import AniLink from "gatsby-plugin-transition-link/AniLink"

const Logo = styled.img`
  width: 80px;
  height: 80px;
  padding-top: 20px;
  padding-bottom: 20px;
  margin: 0;
  object-position: left;
  object-fit: contain;
`

const NavigationBar = styled.nav`
  width: 100%;
  height: 80px;
  /* background: red; */
  margin-bottom: 32px;
`
const NavInner = styled.div`
  width: 100%;
  height: 80px;
  max-width: ${theme.maxWidth};
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-right: ${theme.gutter};
  padding-left: ${theme.gutter};
`

const NavItemContainer = styled.div`
  display: flex;
  a {
    border-color: unset;
    box-shadow: unset;
    outline: 0 none;
    margin-right: 16px;
    color: #fff;
    text-decoration: none;
    padding-bottom: 0px;
    position: relative;
    opacity: 0.4;
    transition: 0.4s;
    &:last-child {
      margin-right: 0;
    }
    &:after {
      width: 4px;
      height: 4px;
      content: "";
      background: #fff;
      position: absolute;
      bottom: -4px;
      left: 0;
      right: 0;
      border-radius: 50%;
      margin: auto;
      transform: scale(0);
      transform-origin: center;
      transition: 0.4s;
    }
    &:hover {
      opacity: 0.6;
      &:after {
        transform: scale(1);
      }
    }
    &.active {
      opacity: 1;
      &:after {
        transform: scale(1);
      }
    }
  }
`

const MediaFeature = props => {
  return (
    <>
      <NavigationBar>
        <NavInner>
          <AniLink fade duration={0.2} to="/">
            <Logo src={LogoImg}></Logo>
          </AniLink>
          <NavItemContainer>
            <AniLink fade duration={0.2} activeClassName="active" to="/">
              Home
            </AniLink>
            <AniLink
              fade
              duration={0.2}
              activeClassName="active"
              to="/projects/"
            >
              Projects
            </AniLink>
            <AniLink fade duration={0.2} activeClassName="active" to="/blog/">
              Blog
            </AniLink>
            <AniLink
              fade
              duration={0.2}
              activeClassName="active"
              to="/contact/"
            >
              Contact
            </AniLink>
          </NavItemContainer>
        </NavInner>
      </NavigationBar>
    </>
  )
}

export default MediaFeature

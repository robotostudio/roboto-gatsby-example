import React from "react"
import styled from "@emotion/styled"
import theme from "../styles/theme"

import Wordpress from "../images/partners/wordpress.png"
import Siteground from "../images/partners/siteground.png"

const StackFeatureContainer = styled.div`
  width: 100%;
  max-width: ${theme.maxWidth};
  padding: ${theme.gutter};
  align-items: center;
  margin-left: auto;
  margin-right: auto;
  @media (min-width: ${theme.breakpoints.md}) {
    display: grid;
    grid-gap: 32px;
    grid-auto-flow: dense;
    align-items: flex-start;
    grid-template-columns: 2fr 3fr;
  }
`
const StackDescription = styled.div`
  @media (max-width: ${theme.breakpoints.md}) {
    margin-bottom: 16px;
  }
`
const StackFeatureText = styled.div`
  width: 100%;
  ol {
    counter-reset: stackCount;
    margin: 0;
    li {
      list-style: none;
      padding-left: 56px;
      position: relative;
      &:before {
        content: counter(stackCount);
        position: absolute;
        color: ${theme.colors.hotPink};
        counter-increment: stackCount;
        left: 0;
        top: 0;
        width: 32px;
        height: 32px;
        text-align: center;
        line-height: 32px;
      }
      &:after {
        content: "";
        position: absolute;
        border: ${theme.colors.hotPink} solid 1px;
        width: 32px;
        height: 32px;
        border-radius: 32px;
        left: 0;
        top: 0;
      }
    }
  }
`

const StackImages = styled.div`
  img {
    max-height: 24px;
    opacity: 0.4;
    + img {
      margin-left: 16px;
    }
  }
`

const StackFeature = props => {
  return (
    <>
      <StackFeatureContainer>
        <StackDescription>
          <StackImages>
            <img src={Wordpress} alt="Wordpress Logo" />
            <img src={Siteground} alt="Siteground Logo" />
          </StackImages>
          <h2>Startup Bundle</h2>
          <p>
            This is for those that want to dip their toes into having a website.
            We created this package just for you. We think it’s the best bang
            for your buck when it comes to a first website.
          </p>
          <p>
            We combine the flexibility of Wordpress with the sheer speed &
            scalibility of the Siteground hosting packs. This bundle is built to
            give you the best possible startup... So we called it the Startup
            bundle
          </p>
        </StackDescription>
        <StackFeatureText>
          <ol>
            <li>
              We start by setting up a collaborative document that you fill with
              all the content you can to help us understand your vision
            </li>
            <li>
              Then we create a barebones brand guidelines, purely to have a
              baseline to work from when it comes to colors and typography
            </li>
            <li>
              We then build out a skeletal website, with a basic layout for
              where objects are on the page
            </li>
            <li>
              We continue to add content and tailor SEO driven copy to populate
              your pages.
            </li>
          </ol>
        </StackFeatureText>
      </StackFeatureContainer>
    </>
  )
}

export default StackFeature

/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import Footer from "./Footer"
import styled from "@emotion/styled"

const LayoutContainer = styled.div`
  > * + * {
    margin-top: 32px;
  }
`

const Layout = ({ children }) => {
  return (
    <>
      <LayoutContainer>{children}</LayoutContainer>
      <Footer></Footer>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout

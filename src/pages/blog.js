import React from "react"
import { graphql } from "gatsby"
import styled from "@emotion/styled"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Nav from "../components/Nav"
import theme from "../styles/theme"
import Img from "gatsby-image"
import AniLink from "gatsby-plugin-transition-link/AniLink"

const BlogListContainer = styled.div`
  display: grid;
  padding-left: ${theme.gutter};
  padding-right: ${theme.gutter};
  max-width: ${theme.maxWidth};
  margin: 0 auto;
  grid-gap: 32px;
  @media (min-width: ${theme.maxWidth}) {
    grid-template-columns: repeat(2, 1fr);
  }
`

const BlogSingleContent = styled.div`
  &:first-of-type {
    @media (min-width: ${theme.maxWidth}) {
      grid-column: 1 / 3;
    }
  }
`

const BlogSingleImage = styled.div`
  height: 480px;
  margin-bottom: 16px;
  @media (max-width: ${theme.breakpoints.lg}) {
    height: 320px;
  }
  @media (max-width: ${theme.breakpoints.sm}) {
    height: 240px;
  }
  .gatsby-image-wrapper {
    height: 100%;
  }
`

export default ({ data }) => {
  console.log(data)
  return (
    <>
      <Nav></Nav>
      <SEO title="Blog" />
      <Layout>
        <BlogListContainer>
          {data.allPrismicPost.edges.map(({ node }) => (
            <BlogSingleContent key={node.uid}>
              <BlogSingleImage>
                <Img
                  fluid={
                    node.data.feature_image.localFile.childImageSharp.fluid
                  }
                ></Img>
              </BlogSingleImage>
              <AniLink
                paintDrip
                hex={theme.colors.hotPink}
                duration={0.5}
                to={"/blog/" + node.uid}
              >
                <h1>{node.data.title.text}</h1>
                <p>{node.data.excerpt.text}</p>
              </AniLink>
            </BlogSingleContent>
          ))}
        </BlogListContainer>
      </Layout>
    </>
  )
}

export const query = graphql`
  query PostList {
    allPrismicPost(sort: { fields: [data___date], order: DESC }) {
      edges {
        node {
          uid
          data {
            title {
              text
            }
            excerpt {
              text
            }
            content {
              text
            }
            feature_image {
              alt
              localFile {
                childImageSharp {
                  fluid(maxWidth: 1024) {
                    ...GatsbyImageSharpFluid_withWebp
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`

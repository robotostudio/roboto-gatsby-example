import React from "react"
import styled from "@emotion/styled"

import theme from "../styles/theme"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Nav from "../components/Nav"
import { navigateTo } from "gatsby-link"

const FormContainer = styled.div`
  max-width: ${theme.maxWidth};
  margin: 0 auto;
  padding-right: ${theme.gutter};
  padding-left: ${theme.gutter};
`
const FormContainerInner = styled.div`
  max-width: 600px;
`
const TextInput = styled.input`
  width: 100%;
  display: block;
  background: rgba(255, 255, 255, 0.1);
  padding: 8px 8px;
  border: none;
  border-radius: 4px;
  margin-top: 8px;
  margin-bottom: 16px;
  color: #fff;
  &::placeholder {
    color: rgba(255, 255, 255, 0.4);
  }
`
const TextAreaInput = styled.textarea`
  width: 100%;
  display: block;
  background: rgba(255, 255, 255, 0.1);
  padding: 8px 8px;
  border-radius: 4px;
  margin-top: 8px;
  margin-bottom: 16px;
  border: none;
  color: #fff;
  &::placeholder {
    color: rgba(255, 255, 255, 0.4);
  }
`

const SubmitButton = styled.input`
  width: 100%;
  max-width: 320px;
  display: block;
  background: ${theme.colors.hotPink};
  color: #fff;
  padding: 8px 8px;
  border-radius: 4px;
  border: none;
  margin-top: 32px;
`

function encode(data) {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
    .join("&")
}

export default class ContactPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleSubmit = e => {
    e.preventDefault()
    const form = e.target
    fetch("/", {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: encode({
        "form-name": form.getAttribute("name"),
        ...this.state,
      }),
    })
      .then(() => navigateTo(form.getAttribute("action")))
      .catch(error => alert(error))
  }

  render() {
    return (
      <>
        <Nav></Nav>
        <SEO title="Contact" />
        <Layout>
          <FormContainer>
            <FormContainerInner>
              <h1>We’re always interested in hearing about your projects</h1>
              <p>Prefer to email us? Send one across to hello@roboto.studio</p>
              <form
                name="contact"
                method="post"
                action="/thanks/"
                data-netlify="true"
                data-netlify-honeypot="bot-field"
                onSubmit={this.handleSubmit}
              >
                {/* The `form-name` hidden field is required to support form submissions without JavaScript */}
                <input type="hidden" name="form-name" value="contact" />
                <div hidden>
                  <label>
                    Don’t fill this out:{" "}
                    <input name="bot-field" onChange={this.handleChange} />
                  </label>
                </div>

                {/* Client Name */}
                <label className="form__label" htmlFor="name">
                  Name
                </label>
                <TextInput
                  type="text"
                  name="name"
                  id="name"
                  className="form__input"
                  placeholder="Your name"
                  onChange={this.handleChange}
                />

                {/* Client Number */}
                <label className="form__label" htmlFor="number">
                  Number
                </label>
                <TextInput
                  type="telephone"
                  name="number"
                  id="number"
                  className="form__input"
                  placeholder="A phone number so we can get in touch"
                  onChange={this.handleChange}
                />

                {/* Client Email */}
                <label className="form__label" htmlFor="email">
                  Email
                </label>
                <TextInput
                  type="email"
                  name="email"
                  id="email"
                  className="form__input"
                  placeholder="Your email address"
                  onChange={this.handleChange}
                />

                {/* Client Message */}
                <label className="form__label" htmlFor="message">
                  Message
                </label>
                <TextAreaInput
                  name="message"
                  id="message"
                  className="form__textarea"
                  placeholder="Enter some more details"
                  onChange={this.handleChange}
                />

                {/* Form submit button */}
                <SubmitButton
                  type="submit"
                  value="Send Message"
                  className="form__button"
                />
              </form>
            </FormContainerInner>
          </FormContainer>
        </Layout>
      </>
    )
  }
}

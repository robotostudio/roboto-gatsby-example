import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import IndexHero from "../components/IndexHero"
import MediaFeature from "../components/MediaFeature"
import CompanyGrid from "../components/CompanyGrid"
import CustomStack from "../components/CustomStack"
import StartupStack from "../components/StartupStack"
import Nav from "../components/Nav"

const IndexPage = () => (
  <>
    <Nav></Nav>
    <Layout dark>
      <SEO title="Home" />
      <IndexHero></IndexHero>
      <MediaFeature filename="wireframes.png" alt="A set of wireframes">
        <h2>UI Design &amp; Development</h2>
        <p>
          We’re not a full service agency, we’re a specialised Studio in UI
          design &amp; development. We do two things incredibly well:
        </p>
        <ol>
          <li>
            We design and optimise the UI/UX of websites, apps and experiences.
          </li>
          <li>
            We develop websites, apps and experiences with the best tech
            available
          </li>
        </ol>
        <p>
          We’re big believers in sticking to what you’re best at, which is why
          we pair with specialised agencies to ensure that all our work is
          industry leading.
        </p>
      </MediaFeature>
      <MediaFeature
        reverse
        filename="prismic.png"
        alt="Two coding windows with prismic"
      >
        <h2>No cookie cutter solutions</h2>
        <p>
          We partner with the Prismic team to provide you with a truly custom
          solution to your niche problems. Forget having restraints within your
          projects such as: ‘Can you use our CMS to populate our app’. No matter
          how unique your build is, we’ve got an answer.
        </p>
      </MediaFeature>
      <MediaFeature
        filename="locals.png"
        alt="A bunch of dorks at a hack event"
      >
        <h2>Supporting the locals</h2>
        <p>
          Everyone loves a good success story, so why don’t we make one
          together. We’ve made a commitment to work with local businesses and
          charities to give back to the community and really put Nottingham on
          the map
        </p>
      </MediaFeature>
      <StartupStack />
      <CustomStack />
      <CompanyGrid>
        <h2>We work with some pretty big names</h2>
        <p>
          From Fintech blockchain and public procurement businesses, to
          community lead skateparks, we’re a pretty versatile bunch.
        </p>
      </CompanyGrid>
    </Layout>
  </>
)

export default IndexPage

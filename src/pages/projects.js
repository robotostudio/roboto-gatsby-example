import React from "react"
import Img from "gatsby-image"
import { graphql } from "gatsby"
import styled from "@emotion/styled"
import AniLink from "gatsby-plugin-transition-link/AniLink"

import theme from "../styles/theme"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Nav from "../components/Nav"

const ProjectListContainer = styled.div`
  display: grid;
  padding-left: ${theme.gutter};
  padding-right: ${theme.gutter};
  max-width: ${theme.maxWidth};
  margin: 0 auto;
  grid-gap: 32px;
  @media (min-width: ${theme.maxWidth}) {
    grid-template-columns: repeat(2, 1fr);
  }
`

const ProjectSingleContent = styled.div`
  &:first-of-type {
    @media (min-width: ${theme.maxWidth}) {
      grid-column: 1 / 3;
    }
  }
`
const ProjectSingleImage = styled.div`
  height: 480px;
  margin-bottom: 16px;
  @media (max-width: ${theme.breakpoints.lg}) {
    height: 320px;
  }
  @media (max-width: ${theme.breakpoints.sm}) {
    height: 240px;
  }
  .gatsby-image-wrapper {
    height: 100%;
  }
`

export default ({ data }) => {
  console.log(data)
  return (
    <>
      <Nav></Nav>
      <SEO title="Projects" />
      <Layout>
        <ProjectListContainer>
          {data.allPrismicProject.edges.map(({ node }) => (
            <ProjectSingleContent key={node.uid}>
              <ProjectSingleImage>
                <Img
                  fluid={
                    node.data.feature_image.localFile.childImageSharp.fluid
                  }
                ></Img>
              </ProjectSingleImage>
              <AniLink
                paintDrip
                hex={theme.colors.hotPink}
                duration={0.5}
                to={"/projects/" + node.uid}
              >
                <h1>{node.data.title.text}</h1>
                <p>{node.data.excerpt.text}</p>
              </AniLink>
            </ProjectSingleContent>
          ))}
        </ProjectListContainer>
      </Layout>
    </>
  )
}

export const query = graphql`
  query ProjectList {
    allPrismicProject(sort: { fields: [data___date], order: DESC }) {
      edges {
        node {
          uid
          data {
            title {
              text
            }
            excerpt {
              text
            }
            content {
              text
            }
            feature_image {
              alt
              localFile {
                childImageSharp {
                  fluid(maxWidth: 1024) {
                    ...GatsbyImageSharpFluid_withWebp
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`

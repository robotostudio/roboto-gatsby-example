const theme = {
  colors: {
    grape: "#8224E3",
    hotPink: "#FF0068",
    durple: "#170728",
    lavendar: "#DCBBFF",
  },
  maxWidth: "1200px",
  maxWidthText: "720px",
  gutter: "16px",
  breakpoints: {
    xs: "400px",
    sm: "600px",
    md: "900px",
    lg: "1200px",
    xl: "1600px",
  },
}

export default theme

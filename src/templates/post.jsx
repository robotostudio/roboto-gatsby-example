import React from "react"
import { graphql } from "gatsby"
import styled from "@emotion/styled"
import Img from "gatsby-image"
import Arrow from "../images/arrow.svg"
import AniLink from "gatsby-plugin-transition-link/AniLink"

import Nav from "../components/Nav"
import SEO from "../components/seo"
import Footer from "../components/Footer"
import theme from "../styles/theme"

const BackArrow = styled.img`
  position: relative;
  display: inline-block;
  margin: 0;
  transition: 0.4s;
`

const BackLinkContainer = styled.div`
  max-width: ${theme.maxWidth};
  padding: ${theme.gutter};
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 32px;
  position: relative;
  opacity: 0.4;
  &:hover {
    img {
      transform: translate(-10px, 0);
    }
  }
  a {
    padding-left: 8px;
    display: inline-block;
  }
`

const PostSingleContent = styled.div`
  max-width: ${theme.maxWidth};
  p {
    max-width: ${theme.maxWidthText};
    margin-left: auto;
    margin-right: auto;
  }
  p.block-img {
    max-width: unset;
  }
  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    max-width: ${theme.maxWidthText};
    margin-left: auto;
    margin-right: auto;
  }
  pre {
    max-width: ${theme.maxWidthText};
    margin-left: auto;
    margin-right: auto;
  }
  img {
    width: 100%;
  }
`
const PostSingleContainer = styled.div`
  max-width: ${theme.maxWidth};
  padding: 0 ${theme.gutter};
  margin: 0 auto;
`
const PostSingleImage = styled.div`
  height: 480px;
  max-width: ${theme.maxWidth};
  margin-bottom: 16px;
  @media (max-width: ${theme.breakpoints.lg}) {
    height: 320px;
  }
  @media (max-width: ${theme.breakpoints.sm}) {
    height: 240px;
  }
  .gatsby-image-wrapper {
    height: 100%;
  }
`

// Details section
const PostSingleDetails = styled.div`
  max-width: ${theme.maxWidthText};
  margin-right: auto;
  margin-left: auto;
  margin-bottom: 64px;
  text-align: center;
`

const Post = ({ data: { prismicPost } }) => {
  const { data } = prismicPost
  return (
    <>
      <SEO title={data.title.text} />
      <Nav></Nav>
      <BackLinkContainer>
        <BackArrow src={Arrow} alt="Arrow" />
        <AniLink fade duration={0.4} activeClassName="active" to="/blog/">
          Go back to blog
        </AniLink>
      </BackLinkContainer>
      <PostSingleContainer>
        <PostSingleDetails>
          <h1>{data.title.text}</h1>
        </PostSingleDetails>
        <PostSingleImage>
          <Img fluid={data.feature_image.localFile.childImageSharp.fluid}></Img>
        </PostSingleImage>
        <PostSingleContent
          dangerouslySetInnerHTML={{ __html: data.content.html }}
        />
      </PostSingleContainer>
      <Footer />
    </>
  )
}

export default Post

export const pageQuery = graphql`
  query PostBySlug($uid: String!) {
    prismicPost(uid: { eq: $uid }) {
      uid
      data {
        date
        content {
          html
        }
        title {
          text
        }
        feature_image {
          alt
          localFile {
            childImageSharp {
              fluid(maxWidth: 1024) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
      }
    }
  }
`

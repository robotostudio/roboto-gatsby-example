import React from "react"
import Img from "gatsby-image"
import { graphql } from "gatsby"
import styled from "@emotion/styled"

import SEO from "../components/seo"
import Nav from "../components/Nav"
import Footer from "../components/Footer"
import theme from "../styles/theme"

const ProjectSingleContent = styled.div`
  max-width: ${theme.maxWidth};
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  p {
    max-width: ${theme.maxWidthText};
    margin-left: auto;
    margin-right: auto;
  }
  p.block-img {
    max-width: unset;
  }
  img {
    width: 100%;
  }
  iframe {
    max-width: 720px;
    width: 100%;
    height: 400px;
    display: block;
    margin-right: auto;
    margin-left: auto;
    @media (max-width: ${theme.breakpoints.lg}) {
      height: 360px;
    }
    @media (max-width: ${theme.breakpoints.sm}) {
      height: 280px;
    }
  }
`
const ProjectSingleContainer = styled.div`
  max-width: ${theme.maxWidth};
  padding: 0 ${theme.gutter};
  margin: 0 auto;
`
const ProjectSingleImage = styled.div`
  height: 480px;
  max-width: ${theme.maxWidth};
  margin-bottom: 16px;
  @media (max-width: ${theme.breakpoints.lg}) {
    height: 320px;
  }
  @media (max-width: ${theme.breakpoints.sm}) {
    height: 240px;
  }
  .gatsby-image-wrapper {
    height: 100%;
  }
`

// Details section
const ProjectSingleDetails = styled.div`
  max-width: ${theme.maxWidthText};
  margin-right: auto;
  margin-left: auto;
  margin-bottom: 64px;
  @media (min-width: ${theme.breakpoints.lg}) {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    h1 {
      grid-column: 1 / 3;
      margin-bottom: 0;
    }
  }
`
const ProjectSingleStack = styled.div`
  * + * {
    margin-top: 8px;
  }
  h3 {
    margin-bottom: 0;
  }
`
const ProjectTags = styled.div``
const ProjectTimeSpan = styled.h3``
const ProjectLink = styled.a`
  color: ${theme.colors.hotPink};
`

const Project = ({ data: { prismicProject } }) => {
  const { data } = prismicProject
  return (
    <>
      <SEO title={data.title.text} />
      <Nav />
      <ProjectSingleContainer>
        <ProjectSingleImage>
          <Img
            fluid={data.feature_image.localFile.childImageSharp.fluid}
            alt={data.feature_image}
          />
        </ProjectSingleImage>
        <ProjectSingleDetails>
          <h1>{data.excerpt.text}</h1>
          <ProjectSingleStack>
            <ProjectLink href={data.website_link.url}>
              {data.website_link.url}
            </ProjectLink>
            <ProjectTags>Infrastructure, Development</ProjectTags>
            <ProjectTimeSpan>{data.time.text}</ProjectTimeSpan>
          </ProjectSingleStack>
        </ProjectSingleDetails>
        <ProjectSingleContent
          dangerouslySetInnerHTML={{ __html: data.content.html }}
        />
      </ProjectSingleContainer>
      <Footer />
    </>
  )
}

export default Project

export const pageQuery = graphql`
  query ProjectBySlug($uid: String!) {
    prismicProject(uid: { eq: $uid }) {
      uid
      data {
        date
        content {
          html
        }
        title {
          text
        }
        excerpt {
          text
        }
        tags {
          tag
        }
        time {
          text
        }
        website_link {
          url
        }
        feature_image {
          alt
          localFile {
            childImageSharp {
              fluid(maxWidth: 2048) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
      }
    }
  }
`
